package main

import (
	"bufio"
	"fmt"
	"net"
	"os"
	"strconv"
	"strings"
)

var numberClients = 0

func handlerConcurrentConnection(connection *net.TCPConn) {
	fmt.Print("")
	for {
		var netData, err = bufio.NewReader(connection).ReadString('\n')

		if err != nil {
			fmt.Println(err)
			return
		}

		messageFromClient := strings.TrimSpace(string(netData))
		if strings.ToLower(messageFromClient) == "logout" {
			numberClients--
			break
		}

		fmt.Println(
			messageFromClient,
			"( "+connection.RemoteAddr().String()+" )",
		)

		counter := strconv.Itoa(numberClients) + "\n"
		connection.Write([]byte("current users: " + counter))
	}
	connection.Close()
}

func main() {
	arguments := os.Args
	if len(arguments) == 1 {
		fmt.Println("Please provide a port number!")
		return
	}

	PORT, errConversion := strconv.Atoi(arguments[1])
	if errConversion != nil {
		fmt.Println(errConversion)
		return
	}
	var addrServer = net.TCPAddr{
		IP:   net.IPv4(127, 0, 0, 1),
		Port: PORT,
		Zone: "",
	}
	serverListener, err := net.ListenTCP("tcp4", &addrServer)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer serverListener.Close()

	for {
		newConnection, err := serverListener.AcceptTCP()
		if err != nil {
			fmt.Println(err)
			return
		}
		go handlerConcurrentConnection(newConnection)
		numberClients++
	}
	/*
		code for single threaded server - blocking
		c, err := l.AcceptTCP()
		for {
			netData, err := bufio.NewReader(c).ReadString('\n')
			if err != nil {
				fmt.Println(err)
				return
			}
			if strings.TrimSpace(netData) == "STOP" {
				fmt.Println("Exiting TCP server!")
				return
			}

			fmt.Print("-> ", netData)
			t := time.Now()
			timeConnection := t.Format(time.RFC3339) + "\n"
			c.Write([]byte(timeConnection))
		}*/
}
