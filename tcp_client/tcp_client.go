package main

import (
	"bufio"
	"fmt"
	"net"
	"os"
	"strings"
)

func main() {

	arguments := os.Args
	USERNAME := "unknown"
	if len(arguments) > 1 {
		USERNAME = arguments[1]
	}

	var connection, err = net.Dial("tcp4", ":1234")
	if err != nil {
		fmt.Println(err)
		return
	}

	for {
		reader := bufio.NewReader(os.Stdin)
		fmt.Print(">> ")
		text, _ := reader.ReadString('\n')
		fmt.Fprintf(connection, USERNAME+": "+text+"\n")

		message, _ := bufio.NewReader(connection).ReadString('\n')
		fmt.Print("->: " + message)
		if strings.TrimSpace(strings.ToLower(text)) == "logout" {
			fmt.Println("TCP client exiting...")
			return
		}
	}
}
